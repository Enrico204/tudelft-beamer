# tudelft-beamer-2022


A PDF containing the slides can be created using the following command: pdflatex presentation.tex

Alternatively, the LaTeX editor Texmaker can be used (http://www.xm1math.net/texmaker), which is available for Windows, OS X and Linux.

Make sure that your LaTeX distribution has been properly installed (e.g., TeX Live, MiKTeX).

https://ctan.org/pkg/gnuplottex
https://ctan.org/pkg/pgfplots
https://asymptote.sourceforge.io/gallery/index.html

https://www.tudelft.nl/huisstijl/merk/merkarchitectuur

https://www.tudelft.nl/huisstijl/middelen/presentaties

https://matplotlib.org/stable/gallery/index.html#pie-and-polar-charts

https://matplotlib.org/stable/gallery/index.html#lines-bars-and-markers



To do:
- [ ] https://fonts.google.com/?query=roboto
- [ ] https://fonts.google.com/specimen/Roboto+Slab
- [ ] https://fonts.google.com/specimen/Roboto
- [ ] docker compose file for jeroen

- [ ] https://www.tudelft.nl/huisstijl/middelen/iconen

% https://tex.stackexchange.com/questions/258/what-is-the-difference-between-let-and-def
% https://tex.stackexchange.com/questions/88001/when-to-use-letltxmacro

https://pgfplots.sourceforge.net/gallery.html
ctan pgfplots
https://ctan.org/pkg/pgfplots?lang=en

https://www.overleaf.com/learn/latex/Pgfplots_package

## Test and Deploy

docker build --tag=$(basename $(pwd)):$(date "+DATE: %Y-%m-%d")_001 .                       

docker run -v $(pwd):/latex_course -it $(basename $(pwd)):$(date "+DATE: %Y-%m-%d")_001 /bin/bash

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.



Customize subtitle:
https://tex.stackexchange.com/questions/187674/beamer-how-to-format-both-frametitle-and-framesubtitle
https://tex.stackexchange.com/questions/144388/how-to-add-a-subtitle-to-a-beamer
https://tex.stackexchange.com/questions/68347/different-styles-of-bullets-of-enumerate
https://tex.stackexchange.com/questions/2291/how-do-i-change-the-enumerate-list-format-to-use-letters-instead-of-the-defaul

https://latex-beamer.com/tutorials/beamer-themes/
https://www.overleaf.com/learn/latex/Beamer%23Creating_a_table_of_contents
https://ctan.org/pkg/beamer
Huisstijl
https://tex.stackexchange.com/questions/43558/redefining-enumerate-counters-in-beamer

% https://tex.stackexchange.com/questions/178800/creating-sections-each-with-title-pages-in-beamers-slides
% https://stackoverflow.com/questions/75200512/how-to-customize-the-title-page-of-beamer-presentation

  % \listfiles
  % https://www.texdev.net/2016/11/30/dependencies/
  % https://tex.stackexchange.com/questions/265726/embed-nicely-formatted-listfiles-into-document



https://tex.stackexchange.com/questions/178800/creating-sections-each-with-title-pages-in-beamers-slides
https://stackoverflow.com/questions/75200512/how-to-customize-the-title-page-of-beamer-presentation

Customize subtitle:
https://tex.stackexchange.com/questions/187674/beamer-how-to-format-both-frametitle-and-framesubtitle
https://tex.stackexchange.com/questions/144388/how-to-add-a-subtitle-to-a-beamer
https://tex.stackexchange.com/questions/68347/different-styles-of-bullets-of-enumerate
https://tex.stackexchange.com/questions/2291/how-do-i-change-the-enumerate-list-format-to-use-letters-instead-of-the-defaul

https://latex-beamer.com/tutorials/beamer-themes/
https://www.overleaf.com/learn/latex/Beamer%23Creating_a_table_of_contents
https://ctan.org/pkg/beamer

Huisstijl
https://tex.stackexchange.com/questions/43558/redefining-enumerate-counters-in-beamer

https://pgfplots.net/
https://texample.net/tikz/examples/

FROM registry.gitlab.com/novanext/base-docker:master

# collection for a package can be found by searching, e.g. for "depend siunitx"
# http://ctan.triasinformatica.nl/systems/texlive/tlnet/tlpkg/texlive.tlpdb
# ADD ./texlive.profile /texlive.profile

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update \
    && apt-get install -y cabextract default-jre inkscape libfontconfig make perl python-is-python3 unzip wget
RUN wget https://www.freedesktop.org/software/fontconfig/webfonts/webfonts.tar.gz \
    && tar -xzf webfonts.tar.gz && cd msfonts && cabextract *.exe && cd .. \
    && cp -r msfonts /usr/share/fonts/truetype
RUN tlmgr update --self \
    && tlmgr install arara
 # texlive-extra-utils
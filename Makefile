
.PHONY: all clean force-build
# to avoid confusion from e.g. zsh
SHELL=/bin/bash

# define default targets for `make
all: presentation.pdf

clean:
	rm -f *.aux
	rm -f *.bbl
	rm -f *.bcf
	rm -f *.blg
	rm -f *.fdb_latexmk
	rm -f *.fls
	rm -f *.log
	rm -f *.nav
	rm -f *.out
	rm -f *.run.xml
	rm -f *.snm
	rm -f *.synctex.gz
	rm -f *.toc
	rm -f *.vrb
	rm -f *.xml

# this target is never made, listing it as a dependency forces the make tasks
# below to be always executed.
force-build:

presentation.pdf: force-build icons graphs
	(arara presentation -v && exit) ||: # continue of arara could not be found
	pdflatex -draftmode -interaction=nonstopmode presentation
	biber --isbn-normalise presentation # see https://www.mankier.com/1/biber
	pdflatex -interaction=nonstopmode presentation

# https://wiki.inkscape.org/wiki/Using_the_Command_Line
icons:
	wget -O icons.zip https://filelist.tudelft.nl/Websections/Huisstijl/ICONS.zip
	unzip -uo icons.zip # update and overwrite
	rm -rf icons
	mv "ICONENSET/RGB : online/" icons/
	for d in icons/*; \
	  do cd "$$d"; \
	  for f in *.svg; \
	    do inkscape "$$f" --export-filename="$${f/.svg/.pdf}"; \
	  done; \
	  cd -; \
	done
	rm -rf ICONENSET

# https://docs.gitlab.com/ee/ci/jobs/job_artifacts.html#from-a-url
# project id 8287077: novanext/tudelft-dissertation
graphs:
	wget -O graphs.zip 'https://gitlab.com/api/v4/projects/8287077/jobs/artifacts/new_colours/download?job=render_graphs'
	unzip graphs.zip
	mkdir $@
	mv _python/* $@
	rm -r _python
